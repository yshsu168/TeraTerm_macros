## Purpose:
  The collection of TeramTerm macro that I created/modified for automatcion testing.

## Simple description of the scripts
- reboot_brcm.ttl: The macro to ping LAN side PC and reboot the Broadcom DUT once successfully in loop.
- reboot_starlite.ttl: The macro to ping LAN side PC and reboot the StarLite DUT once successfully in loop.
- reboot_starlite_cfe.ttl: The macro to enter StarLite DUT's CFE mode and check if the PC sent http request.
